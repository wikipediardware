/* This file is generated from sys_rename.def by genrename. */

#ifdef _SYS_RENAME_H_
#undef _SYS_RENAME_H_

/*
 * sys_config.h
 */
#undef sys_initialize
#undef sys_exit
#undef get_Offset
#undef get_BitFlag

#ifdef LABEL_ASM

/*
 * sys_config.h
 */
#undef _sys_initialize
#undef _sys_exit
#undef _get_Offset
#undef _get_BitFlag

#endif /* LABEL_ASM */
#endif /* _SYS_RENAME_H_ */
