#ifndef E07_H
#define E07_H

int sync_cpu(int fd);
int bootstrap(int ttyfd, const char *bootstrap_file);

#endif /* E07_H */

