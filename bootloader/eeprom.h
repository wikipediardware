#ifndef EEPROM_H
#define EEPROM_H

void eeprom_load(u32 addr, u8 *dest, u32 size);

#endif /* EEPROM_H */
