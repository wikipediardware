"""
 Parse a glyphmap.map generated by gen_font_file.py

 Copyright (C) 2009 Holger Hans Peter Freyther <zecke@openmoko.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

def load(file):
    fonts = {}
    for line in open(file):
        split = line.strip().rsplit(' ', 2)
        try:
            font = fonts[split[0]]
        except KeyError:
            fonts[split[0]] = {}
            font = fonts[split[0]]

        font[split[1]] = split[2]

    return fonts
