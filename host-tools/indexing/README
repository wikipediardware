This is using the approach the same approach/code as the wikipedia-iphone reader.

== Approach ==

We will get all titles from the wikipedia dataset and we will get the
offsets from our huge file from somewhere else and then we will have a
file with lines like:
    Title Offset

And this is what we are going to stuff into the locate tools and what
we are going to use.

== Reason ==

Trees/Tries are really good for text retrieval. They have a O(n) performance
characteristic. But our problem is not determing if a certain word exists
in the Titles of Wikipedia but to find the article. This means there will
be a lot (thousands) of references to the articles, meaning you will have to
jump through hoops to make this efficient and not access O(n) blocks during
a simple search.

Locate itself is space efficient using an ancient trick. Basicly when the
titles are sorted the prefix from the current and previous article match. This
is a really good way to save storage.

To back this up with numbers:
-rw-r--r--  1 ich  admin    44M 19 Jan 22:52 index.locate.db
-rw-r--r--  1 ich  admin    87M 19 Jan 22:51 indexfile.index.sorted


== Details ==

1.) get a wikipedia xml document/dump
2.) Compile CreateInex into wiki-tools
3.) run wiki-tools and a indexfile.index will fall out
4.) Sort
    cat indexfile.index | LC_ALL=C sort -u | ./sort.py > indexfile.index.sorted
5.) Generate bigram
    locate/lbigram < indexfile.index.sorted | sort -nr | perl -ne '/^\s*[0-9]+\s(..)$/ && print $1' > indexfile.index.bigrams
6.) Generate index
    locate/lcode indexfile.index.bigrams < indexfile.index.sorted > pedia.idx
7.) Generate a prefix table saying where which char starts...
    locate/lsearcher -f pedia.idx -c pedia.jmp -n
8.) ... merge it into one big content file...

Use a copy of locate and store the titles in this 
