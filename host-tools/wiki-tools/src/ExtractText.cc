/*
 * Wiki Handling Tool
 *
 * Copyright (C) 2008 Openmoko Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ExtractText.h"

#include <QTextStream>

ExtractText::ExtractText(const QString& outputFile)
    : FileOutputArticleHandler(outputFile)
{}

void ExtractText::handleArticle(const Article& text)
{
    if (text.isRedirect())
        return;

    QTextStream stream(&m_file);
    stream << text.textContent();
    stream << "\n\n\n";
}
