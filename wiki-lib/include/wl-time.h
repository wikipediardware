#ifndef WL_TIME_H
#define WL_TIME_H

/* returns the amount of milliseconds since uptime */
unsigned int get_timer(void);

#endif /* WL_TIME_H */

